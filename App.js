import * as React from 'react';
import { TextInput, Button, Card, Appbar,RadioButton, Checkbox, Switch, TouchableRipple } from 'react-native-paper';
import { View, Text, StyleSheet, ScrollView, Alert, Image, Keyboard } from 'react-native';
import { createRef } from 'react';
import DateTimePicker from 'react-native-modal-datetime-picker';



export default class MyComponent extends React.Component {


   constructor(props) {
    super(props);
    this.state = {
      date: undefined,
      open: false,
      name: '', mail: '', mob: '',
    js: false,
    html: false,
    css: false,
    angular: false,
    vue: false,
    react: false,
    node: false,
    express: false,
    meteor: false,
    redis: false,
    mongo: false,
    sql: false,
    user: '',
    value1: '',
    isSwitchOn: false,
    checked: 'male'
   
    };
    this._textInput = createRef();
  }

  handleFocus = () => {
    this._textInput.current._root._handleFocus();
  };

  handleBlur = () => {
    setTimeout(this._textInput.current._root._handleBlur, 100);
  };

  handleChange = date => {
    this.handleClose();
    this.setState({ date });
  };

  handleClose = () => {
    this.setState({ open: false }, this.handleBlur);
  };

  handleOpen = () => {
    Keyboard.dismiss();
    this.handleFocus();
    this.setState({ open: true });
  };

  handlename = (text) => {
    this.setState({ name: text })
  }

  handlemail = (text) => {
    this.setState({ mail: text })
  }

  handlemobile = (text) => {
    this.setState({ mob: text })
  }

  renderTouchText = props => {
    const { style, value } = props;

    return (
      <TouchableRipple onPress={this.handleOpen}>
        <Text style={style}>{value}</Text>
      </TouchableRipple>
    );
  };

  onPress =() => {
    let data = {
      name: this.state.name,
      mail: this.state.mail,
      DOB: this.state.date,
      mob: this.state.mob,
      Javascript: this.state.js,
      Html5: this.state.html,
      Css3: this.state.css,
      Angular: this.state.angular,
      Vue: this.state.vue,
      React: this.state.react,
      Node: this.state.node,
      Express: this.state.express,
      Meteor: this.state.meteor,
      MariaDB:this.state.redis,
      Mongo:this.state.mongo,
      MySQL:this.state.sql,
      Accept:this.state.isSwitchOn,
      Gender: this.state.value1
}
    
    fetch('http://172.17.4.181:3000/register1', {
                    method: 'POST',
                    headers: {
                      
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data),
                  })
                  .then(res=>Alert.alert("Successfully Registered"))
                  .catch(function (error)
                  {
                    console.warn(error.message);
                  });
                }

  render(){

    const {name, mail, js, html, css, angular, value1, vue, react, node, express, meteor, redis, mongo, sql,  date, open, isSwitchOn  } = this.state;
        const value = date ? date.toDateString() : '';

    return (

      <View>
       <Appbar style={styles.bottom}>
        <Text style={styles.appbar}>Job-Application</Text>
       </Appbar>

       <ScrollView ContentContainerStyle={{ height: 750 }}>

       <Card style={styles.card2}>
      <Card.Content>

      <View style={styles.flexbet}>
        <Text style={styles.text}>Personal Details</Text>
      </View>

      </Card.Content>
      </Card>


      <Card style={styles.card1}>
      <Card.Content>

      <View style={styles.flex}>
      <Image
          style={{width: 120, height: 120}}
          source={{uri: 'https://tlcform.netlify.com/login.png'}}
        />
        </View>

      <Text></Text>

      <View style={styles.flex}>
      <RadioButton.Group
        onValueChange={value => this.setState({ value1:value })}
        value={value1}
      >
        <View style={styles.flex}>
          <Text>Male</Text>
          <RadioButton value="male" color='#e91e63'/>
          <View><Text>                 </Text></View>
        </View>
        <View style={styles.flex}>
          <Text>Female</Text>
          <RadioButton value="female" color='#e91e63'/>
        </View>
      </RadioButton.Group>
      </View>
      <Text></Text>

      <Text></Text>

      <TextInput
        label='Name'
        mode='outlined'
        value={name}
        color='#e91e63'
        onChangeText={this.handlename}
      />


      

      <Text></Text>

      <View style={{ justifyContent:'center' }}>
        <TextInput
          label='Date of Birth'
          mode='outlined'
          ref={this._textInput}
          render={this.renderTouchText}
          value={value}
          color='#e91e63'
        />
        <DateTimePicker
          date={date}
          isVisible={open}
          onConfirm={this.handleChange}
          onCancel={this.handleClose}
          color='#e91e63'
        />
      </View>

      <Text></Text>

      <TextInput
        label='Email'
        mode='outlined'
        value={mail}
        color='#e91e63'
        onChangeText={this.handlemail}
      />

   

      <Text></Text>

      <TextInput
        label='Mobile'
        mode='outlined'
        color='#e91e63'
        keyboardType ='numeric'
        value={this.state.mob}
        onChangeText={this.handlemobile}
      />
      

      <Text></Text>

      </Card.Content>
      </Card>

      <Card style={styles.card2}>
      <Card.Content>


      <View style={styles.flexbet}>
        <Text style={styles.text}>Skill Sets</Text>
      </View>

      </Card.Content>
      </Card>
      

      <Card >
        <Card.Content >
     


      <View style={styles.flexbet}>
        <Text style={styles.text1}>Front End</Text>
      </View>

<View style={styles.flexbet}>
      <View style={styles.flex}>
      <Checkbox
        status={js ? 'checked' : 'unchecked'}
        color='#e91e63'

        onPress={() => { this.setState({ js: !js }); }}
      />
      <Text>Java Script</Text>
</View>
<View style={styles.flex}>
      <Checkbox
        status={html ? 'checked' : 'unchecked'}
        color='#e91e63'
        onPress={() => { this.setState({ html: !html }); }}
      />
      <Text>Html 5</Text>
</View>
<View style={styles.flex}>
      <Checkbox
        status={css ? 'checked' : 'unchecked'}
        color='#e91e63'
        onPress={() => { this.setState({ css: !css }); }}
      />
      <Text>CSS 3</Text>
      </View>
      </View>
      <View style={styles.flexbet}>

      <View style={styles.flex}>
      <Checkbox
        status={angular ? 'checked' : 'unchecked'}
        color='#e91e63'
        onPress={() => { this.setState({ angular: !angular }); }}
      />
      <Text>Angular</Text>
</View>
<View style={styles.flex}>

      <Checkbox
        status={vue ? 'checked' : 'unchecked'}
        color='#e91e63'
        onPress={() => { this.setState({ vue: !vue }); }}
      />
      <Text>Vue</Text>
</View>
<View style={styles.flex}>

      <Checkbox
        status={react ? 'checked' : 'unchecked'}
        color='#e91e63'
        onPress={() => { this.setState({ react: !react }); }}
      />
      <Text>React</Text>
      </View>
</View>
      </Card.Content>
      </Card>

      <Card>
        <Card.Content>

        <View style={styles.flexbet}>
        <Text style={styles.text1}>Back End</Text>
      </View>
<View style={styles.flexbet}>
      <View style={styles.flex}>
      <Checkbox
        status={node ? 'checked' : 'unchecked'}
        color='#e91e63'
        onPress={() => { this.setState({ node: !node }); }}
      />
      <Text>Node js</Text>
      </View>
      <View style={styles.flex}>

      <Checkbox
        status={express ? 'checked' : 'unchecked'}
        color='#e91e63'
        onPress={() => { this.setState({ express: !express }); }}
      />
      <Text>Express js</Text>
      </View>
      <View style={styles.flex}>

      <Checkbox
        status={meteor ? 'checked' : 'unchecked'}
        color='#e91e63'
        onPress={() => { this.setState({ meteor: !meteor }); }}
      />
      <Text>Meteor js</Text>
      </View>

      </View>
       </Card.Content>
      </Card>

      <Card>
        <Card.Content>

        <View style={styles.flexbet}>
        <Text style={styles.text1}>Databases</Text>
      </View>

      <View style={styles.flexbet}>
      <View style={styles.flex}>
      <Checkbox
        status={redis ? 'checked' : 'unchecked'}
        color='#e91e63'
        onPress={() => { this.setState({ redis: !redis }); }}
      />
      <Text>MariaDB</Text>
      </View>
      <View style={styles.flex}>

      <Checkbox
        status={mongo ? 'checked' : 'unchecked'}
        color='#e91e63'
        onPress={() => { this.setState({ mongo: !mongo }); }}
      />
      <Text>MongoDB</Text>
      </View>
      <View style={styles.flex}>

      <Checkbox
        status={sql ? 'checked' : 'unchecked'}
        color='#e91e63'
        onPress={() => { this.setState({ sql: !sql }); }}
      />
      <Text>MySQL</Text>
      </View>

      
      </View>
       </Card.Content>
      </Card>

      <Card>
        <Card.Content>
      <View style={styles.flexleft}>

        <Switch  value={isSwitchOn} color='#e91e63' onValueChange={() => { this.setState({ isSwitchOn: !isSwitchOn }); } } />

      <Text>Accept Terms & Conditions</Text>

      </View>

      
      <Text></Text>   

       <Button  mode="contained" style={styles.b1}
      onPress = {this.onPress}>
        Submit
      </Button>

      <Text></Text>
      <Text></Text>
      <Text></Text>

      </Card.Content>
      </Card>
      </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottom: {
    left: 0,
    right: 0,
    backgroundColor: '#00796B'
  },
  appbar: {
    fontSize: 26,
     textAlign: 'center',
     color: 'white'
  },
  flex: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    b1: {
      backgroundColor: '#e91e63'
      },
      flexver: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
      },
    card1: {
      //backgroundColor: '#66BB6A',
    },

     card2: {
      //backgroundColor: '#A5D6A7',
    },
    text: {
      fontSize: 25
    },
    text1: {
      fontSize: 22
    },
    flexleft: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center'
    },

    flexbet: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
});